#%%
import random
from minesweeper import Minesweeper,Sentence,MinesweeperAI

#%%
filas=4
colum=4
# minas=2
# tablero=Minesweeper(filas,colum,minas)
# tablero.print() #imprime tablero en forma grafica
# print(tablero.board) #imprime tablero construido como una lista de listas


# pro1=Sentence([(0,0),(0,1),(0,2)],3)
# print(pro1)
# print("Son todas minas?",pro1.all_mines())
# print("mines",pro1.known_mines())
# print("Son todas seguras?",pro1.all_safes())
# print("Safes",pro1.known_safes())

# pro2=Sentence([(1,0),(1,1),(1,2)],0)
# print(pro2)
# print("Son todas seguras?",pro2.all_safes())
# print("Safes",pro2.known_safes())
# print("Son todas minas?",pro2.all_mines())
# print("mines",pro2.known_mines())

# pro3=Sentence({(5,3),(6,7)},1)
# print(pro3)
# print("Son todas minas?",pro3.all_mines())
# print("mines",pro3.known_mines())
# print("Son todas seguras?",pro3.all_safes())
# print("safes", pro3.known_safes())

# pro4=Sentence({},0) 
# print(pro4.all_safes())
# print(pro4.all_mines())

# pro1.mark_mine((2,3))
# print(pro1)
# pro1.mark_mine((0,1))
# print(pro1)

# tab1=[[False, True, False], [False, True, False], [False, False, False]]
# for i in tab1:
#     print(i)

#Prueba de la primera parte de adknoledge con un tablero pequeño de 3*3
# mai=MinesweeperAI(filas,colum)
# print("Primer intent")
# mai.add_knowledge((0,0),2)
# print("Movimientos realizados", mai.moves_made)
# print("lo que se")
# for kn in mai.knowledge:
#     print(kn)
    

# print("Minas",mai.mines)
# print("Seguros",mai.safes)

# print("segundo intento")
# mai.add_knowledge((1,0),2)
# print("Movimientos realizados", mai.moves_made)
# print("lo que se")
# for kn in mai.knowledge:
#     print(kn)
    
# print("Minas",mai.mines)
# print("Seguros",mai.safes)

# len_known=len(mai.knowledge)
# p=0
# #removel=[]
# #know_copy=mai.knowledge.copy()
# #este loop recorre todas las afirmaciones del knowledge  y verifica si alguna  de
# #dichas afirmaciones  revla un conjunto de celdas minas o seguras
# #si la afimarcion tiene minas se crea un  frozet de minas  en el cual se itera
# #para marcar las minas en el knoledge original
# #de igual forma pasa si la afimacion tiene celdas seguras
# #si la sentecia no dice nada al respecto  no pasa nada
# for p in range(len_known):
#     prop=mai.knowledge[p]
#     print("analizo la  prop", prop)
#     if prop.all_mines():
#         print("todaas son minas")
#         #removel.append(p)
#         other_mines=frozenset(prop.known_mines())
#         for new_m in other_mines:
#             print("la  marco como mina en la original")
#             mai.mark_mine(new_m)
#             print("mi contador:",other_mines)
#             print('lo que se')
#             for kn in mai.knowledge:
#                 print(kn)
#     elif prop.all_safes():
#         print("todas son sguras")
#         #removel.append(p)
#         other_safes=frozenset(prop.known_safes())
#         for new_s in other_safes:
#             print("la  marco como safe en la original")
#             mai.mark_safe(new_s)
#             print("mi contador:",other_safes)
#             print('lo que se')
#             for kn in mai.knowledge:
#                 print(kn)
        
# print("tama",len(mai.knowledge))
# #print(removel)
# print(type(mai.knowledge))
# newsentences=list()

# #este loop recorre el knowledge para hacer nuevas inferencias... por ahora solo tengo
# #el caso en que el p1 es subo conjunto de p2.
# for p1 in range(len_known):
#     s1=mai.knowledge[p1]
#     for p2 in range(p1+1,len_known):
#         s2=mai.knowledge[p2]
#         if s1.cells.issubset(s2.cells):
#             diff_cell=s2.cells-s1.cells
#             diff_count=s2.count-s1.count
#             newsentences.append(Sentence(diff_cell,diff_count)) 
#         elif s2.cells.issubset(s1.cells):
#             diff_cell=s1.cells-s2.cells
#             diff_count=s1.count-s2.count
#             newsentences.append(Sentence(diff_cell,diff_count))

# len_ns= len(newsentences)
# if len_ns!=0:
#     mai.knowledge.extend(newsentences)
# #Tengo que ver si es posible quitar las afirmaciones vacias de el knowledges

# for inference in newsentences:
#     print("entre")
#     print(inference)
        

##################################
#Pruebas de la primera pate de adknoledge  para un tablero de 4*4
mai=MinesweeperAI(filas,colum)
print("---------------Primer movimiento---------")
mai.add_knowledge((1,2),2)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
    print(kn)
    

print("Minas",mai.mines)
print("Seguros",mai.safes)

print("---------segundo Movimiento---------------------")
mai.add_knowledge((3,1),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

len_known=len(mai.knowledge)
p=0
for p in range(len_known):
    prop=mai.knowledge[p]
    print("analizo la  prop", prop)
    if prop.all_mines():
        print("todaas son minas")
        #removel.append(p)
        other_mines=frozenset(prop.known_mines())
        for new_m in other_mines:
            print("la  marco como mina en la original")
            mai.mark_mine(new_m)
            print("mi contador:",other_mines)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)
    elif prop.all_safes():
        print("todas son sguras")
        #removel.append(p)
        other_safes=frozenset(prop.known_safes())
        for new_s in other_safes:
            print("la  marco como safe en la original")
            mai.mark_safe(new_s)
            print("mi contador:",other_safes)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)

print("Minas",mai.mines)
print("Seguros",mai.safes)

newsentences=list()
for p1 in range(len_known):
    s1=mai.knowledge[p1]
    print("S1: ",s1)
    for p2 in range(p1+1,len_known):
        s2=mai.knowledge[p2]
        print("S2:",s2 )
        if s1.cells.issubset(s2.cells):
            print("encontro un subconjuto 1")
            diff_cell=s2.cells-s1.cells
            diff_count=s2.count-s1.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 

        elif s2.cells.issubset(s1.cells):
            print("encontro un subconjuto 2")
            diff_cell=s1.cells-s2.cells
            diff_count=s1.count-s2.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 


len_ns= len(newsentences)
if len_ns!=0:
    print("hubo inferencias nuevas y las agregare")
    mai.knowledge.extend(newsentences)

print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


print("---------Teercer Movimiento---------------------")

mai.add_knowledge((3,3),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

len_known=len(mai.knowledge)
print("tama de KB:",len_known)
p=0
for p in range(len_known):
    prop=mai.knowledge[p]
    print("analizo la  prop", prop)
    if prop.all_mines():
        print("todaas son minas")
        #removel.append(p)
        other_mines=frozenset(prop.known_mines())
        for new_m in other_mines:
            print("la  marco como mina en la original")
            mai.mark_mine(new_m)
            print("mi contador:",other_mines)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)
    elif prop.all_safes():
        print("todas son sguras")
        #removel.append(p)
        other_safes=frozenset(prop.known_safes())
        for new_s in other_safes:
            print("la  marco como safe en la original")
            mai.mark_safe(new_s)
            print("mi contador:",other_safes)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)

print("Minas",mai.mines)
print("Seguros",mai.safes)

newsentences=list()
for p1 in range(len_known):
    s1=mai.knowledge[p1]
    print("S1: ",s1)
    for p2 in range(p1+1,len_known):
        s2=mai.knowledge[p2]
        print("S2:",s2 )
        if s1.cells.issubset(s2.cells):
            print("encontro un subconjuto 1")
            diff_cell=s2.cells-s1.cells
            diff_count=s2.count-s1.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 

        elif s2.cells.issubset(s1.cells):
            print("encontro un subconjuto 2")
            diff_cell=s1.cells-s2.cells
            diff_count=s1.count-s2.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 


len_ns= len(newsentences)
if len_ns!=0:
    print("hubo inferencias nuevas y las agregare")
    mai.knowledge.extend(newsentences)

print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

print("---------CUARTO Movimiento---------------------")

mai.add_knowledge((0,3),0)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

len_known=len(mai.knowledge)
print("tama de KB:",len_known)
p=0
for p in range(len_known):
    prop=mai.knowledge[p]
    print("analizo la  prop", prop)
    if prop.all_mines():
        print("todaas son minas")
        #removel.append(p)
        other_mines=frozenset(prop.known_mines())
        for new_m in other_mines:
            print("la  marco como mina en la original")
            mai.mark_mine(new_m)
            print("mi contador:",other_mines)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)
    elif prop.all_safes():
        print("todas son sguras")
        #removel.append(p)
        other_safes=frozenset(prop.known_safes())
        for new_s in other_safes:
            print("la  marco como safe en la original")
            mai.mark_safe(new_s)
            print("mi contador:",other_safes)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)

print("Minas",mai.mines)
print("Seguros",mai.safes)

newsentences=list()
for p1 in range(len_known):
    s1=mai.knowledge[p1]
    print("S1: ",s1)
    for p2 in range(p1+1,len_known):
        s2=mai.knowledge[p2]
        print("S2:",s2 )
        if s1.cells.issubset(s2.cells):
            print("encontro un subconjuto 1")
            diff_cell=s2.cells-s1.cells
            diff_count=s2.count-s1.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 

        elif s2.cells.issubset(s1.cells):
            print("encontro un subconjuto 2")
            diff_cell=s1.cells-s2.cells
            diff_count=s1.count-s2.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 


len_ns= len(newsentences)
if len_ns!=0:
    print("hubo inferencias nuevas y las agregare")
    mai.knowledge.extend(newsentences)

print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

print("---------QUINTO Movimiento---------------------")

mai.add_knowledge((0,2),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

len_known=len(mai.knowledge)
print("tama de KB:",len_known)
p=0
for p in range(len_known):
    prop=mai.knowledge[p]
    print("analizo la  prop", prop)
    if prop.all_mines():
        print("todaas son minas")
        #removel.append(p)
        other_mines=frozenset(prop.known_mines())
        for new_m in other_mines:
            print("la  marco como mina en la original")
            mai.mark_mine(new_m)
            print("mi contador:",other_mines)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)
    elif prop.all_safes():
        print("todas son sguras")
        #removel.append(p)
        other_safes=frozenset(prop.known_safes())
        for new_s in other_safes:
            print("la  marco como safe en la original")
            mai.mark_safe(new_s)
            print("mi contador:",other_safes)
            print('lo que se')
            for kn in mai.knowledge:
                print(kn)

print("Minas",mai.mines)
print("Seguros",mai.safes)

newsentences=list()
for p1 in range(len_known):
    s1=mai.knowledge[p1]
    print("S1: ",s1)
    for p2 in range(p1+1,len_known):
        s2=mai.knowledge[p2]
        print("S2:",s2 )
        if s1.cells.issubset(s2.cells):
            print("encontro un subconjuto 1")
            diff_cell=s2.cells-s1.cells
            diff_count=s2.count-s1.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 

        elif s2.cells.issubset(s1.cells):
            print("encontro un subconjuto 2")
            diff_cell=s1.cells-s2.cells
            diff_count=s1.count-s2.count
            infered=Sentence(diff_cell,diff_count)
            if not infered in mai.knowledge:
                print("Afiarmcion inferida",infered)
                newsentences.append(infered) 


len_ns= len(newsentences)
if len_ns!=0:
    print("hubo inferencias nuevas y las agregare")
    mai.knowledge.extend(newsentences)

print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

print("---------SEXTO Movimiento---------------------")

mai.add_knowledge((1,3),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


def detect(conocimientos):

    len_known=len(mai.knowledge)
    print("tama de KB:",len_known)
    #ciclo que recorre la lista knowledge y busca posbiles minas 
    #removel=[]
    changes=False
    for p in range(len_known):
        prop=mai.knowledge[p]
        print("analizo la  prop", prop)
        if prop.all_mines():
            print("todaas son minas")
            #removel.append(p) #indice del elemento a remover
            changes=True
            other_mines=frozenset(prop.known_mines())
            for new_m in other_mines:
                print("la  marco como mina en la original")
                mai.mark_mine(new_m)
                print("mi contador:",other_mines)
                print('lo que se')
                for kn in mai.knowledge:
                    print(kn)
        elif prop.all_safes():
            print("todas son sguras")
            #removel.append(p)
            changes=True
            other_safes=frozenset(prop.known_safes())
            for new_s in other_safes:
                print("la  marco como safe en la original")
                mai.mark_safe(new_s)
                print("mi contador:",other_safes)
                print('lo que se')
                for kn in mai.knowledge:
                    print(kn)
   
    print("Minas",mai.mines)
    print("Seguros",mai.safes)

    #ve si se encontro alguna mina o safe (hubo cambio kn)
    #si hubo cambios en el knoledge base entonces limpiara 
    #la basura que podria surgir de encontrar una mina o un safe
    #es decir, quitara lo conjuntos repetidos u los vacios.
    #Ademas  de haberse encontrado cambio finalmente volvera a buscar
    #Nuevas minas y safes que podieron haber aparecido a raiz de los
    #cambios
    if changes:
        print("hubo cambios...vere si hay clones o vacios")
        clean_kb=[]
        for k1 in range(len_known):
            pp1=mai.knowledge[k1]
            print("reviso", pp1)
            if len(pp1.cells)!=0:
                print("no es vacio")
                isnotrepeted=True
                k2=k1+1
                while isnotrepeted and k2<len_known:
                    pp2=mai.knowledge[k2]
                    if pp1==pp2:
                        isnotrepeted=False
                        print("tiene un clon")
                    k2+=1

                if isnotrepeted:
                    clean_kb.append(pp1)

        mai.knowledge=clean_kb
        print('lo que se')
        for kn in mai.knowledge:
            print(kn)
        
        print("Minas",mai.mines)
        print("Seguros",mai.safes)
        detect(mai)


detect(mai)
print("HARE INFERENCIAS A AHORA")
def do_inference(conocimientos):
    newsentences=list()
    len_known=len(mai.knowledge)
    for p1 in range(len_known):
        s1=mai.knowledge[p1]
        print("S1: ",s1)
        for p2 in range(p1+1,len_known):
            s2=mai.knowledge[p2]
            print("S2:",s2 )
            
            if s1.cells<s2.cells:
                print("encontro un subconjuto 1")
                diff_cell=s2.cells-s1.cells
                diff_count=s2.count-s1.count
                infered=Sentence(diff_cell,diff_count)

                if not infered in mai.knowledge: #OJO REVISAR
                    print("Afiarmcion inferida",infered)
                    newsentences.append(infered) 

            elif s2.cells<s1.cells:
                print("encontro un subconjuto 2")
                diff_cell=s1.cells-s2.cells
                diff_count=s1.count-s2.count
                infered=Sentence(diff_cell,diff_count)
                if not infered in mai.knowledge:
                    print("Afiarmcion inferida",infered)
                    newsentences.append(infered) 

    #podro revisar la lista de new senetences  para ver si se inferio alguna celda segura o mina
    #o desejarlo como esta

    len_ns= len(newsentences)
    if len_ns!=0:
        print("hubo inferencias nuevas y las agregare")
        mai.knowledge.extend(newsentences)
        print("lo que se")
        for kn in mai.knowledge:
            print(kn)
            
        print("Minas",mai.mines)
        print("Seguros",mai.safes)

        detect(mai)
        #OJO PRODIA INVOCAR AQUI LA INFERCIA DE NUEVO DE FORMA RECURSIVA
        #HACER MAS PRUEBAS (IDEAR CIRUNTACIA)
        #doinference(mai)

do_inference(mai)


print("movimientos hechos", mai.moves_made)