import itertools
import random


class Minesweeper():
    """
    Minesweeper game representation
    """

    def __init__(self, height=8, width=8, mines=8):

        # Set initial width, height, and number of mines
        self.height = height
        self.width = width
        self.mines = set()

        # Initialize an empty field with no mines
        self.board = []
        for i in range(self.height):
            row = []
            for j in range(self.width):
                row.append(False)
            self.board.append(row)

        # Add mines randomly
        while len(self.mines) != mines:
            i = random.randrange(height)
            j = random.randrange(width)
            if not self.board[i][j]:
                self.mines.add((i, j))
                self.board[i][j] = True

        # At first, player has found no mines
        self.mines_found = set()

    def print(self):
        """
        Prints a text-based representation
        of where mines are located.
        """
        for i in range(self.height):
            print("--" * self.width + "-")
            for j in range(self.width):
                if self.board[i][j]:
                    print("|X", end="")
                else:
                    print("| ", end="")
            print("|")
        print("--" * self.width + "-")

    def is_mine(self, cell):
        i, j = cell
        return self.board[i][j]

    def nearby_mines(self, cell):
        """
        Returns the number of mines that are
        within one row and column of a given cell,
        not including the cell itself.
        """

        # Keep count of nearby mines
        count = 0

        # Loop over all cells within one row and column
        for i in range(cell[0] - 1, cell[0] + 2):
            for j in range(cell[1] - 1, cell[1] + 2):

                # Ignore the cell itself
                if (i, j) == cell:
                    continue

                # Update count if cell in bounds and is mine
                if 0 <= i < self.height and 0 <= j < self.width:
                    if self.board[i][j]:
                        count += 1

        return count

    def won(self):
        """
        Checks if all mines have been flagged.
        """
        return self.mines_found == self.mines


class Sentence():
    """
    Logical statement about a Minesweeper game
    A sentence consists of a set of board cells,
    and a count of the number of those cells which are mines.
    """

    def __init__(self, cells, count):
        self.cells = set(cells)
        self.count = count

    def __eq__(self, other):
        return self.cells == other.cells and self.count == other.count

    def __str__(self):
        return f"{self.cells} = {self.count}"
    
    #I'm adding two methods to the class Sentece: allmines, allsafe

    def all_mines(self):
        """
        Added method: resturns True  if all cells in self.cells are mines.
        Resturns Falls otherwise
        """
        if  len(self.cells)==self.count and self.count!=0 :
            return True
        else:
            return False
    
    def all_safes(self):
        """
        Added method: returns True if there are no mines in the sentece,
        that is when self.count=0 
        """
        if self.count==0 and len(self.cells)!=0:
            return True
        else:
            return False
    
    def diff_sentence(self,other):
        """
        Method added: returns a knew sentence that resulst from restesting self-other.
        This is new.cells=self.cells - other.cells and new.count=self.count-other.count
        """
        new_s=self.cells-other.cells
        new_c=self.count-other.count
        return Sentence(new_s,new_c)


    def known_mines(self):
        """
        Returns the set of all cells in self.cells known to be mines.
        """
        #I know the set of cells are all mines when the count number is equal to
        #the size of the set of cells
        #REVISAR DE NUEVO
        if self.all_mines():
            return self.cells

    def known_safes(self):
        """
        Returns the set of all cells in self.cells known to be safe.
        """
        #the cells in a set are safe whe the count of the sentence is 0
        if self.all_safes():
            return self.cells
        

    def mark_mine(self, cell):
        """
        Updates internal knowledge representation given the fact that
        a cell is known to be a mine.
        """
        #this check if the cell is self.cells and mark it as mine
        #and updates de sentece
        if cell in self.cells:
            self.cells.discard(cell)
            self.count-=1
            

        

    def mark_safe(self, cell):
        """
        Updates internal knowledge representation given the fact that
        a cell is known to be safe.
        """
        #chek if the cell is in self.cells and mark it as safe 
        #and updates de sentence
        if cell in self.cells:
            self.cells.discard(cell)




class MinesweeperAI():
    """
    Minesweeper game player
    """

    def __init__(self, height=8, width=8):

        # Set initial height and width
        self.height = height
        self.width = width

        # Keep track of which cells have been clicked on
        self.moves_made = set()

        # Keep track of cells known to be safe or mines
        self.mines = set()
        self.safes = set()

        # List of sentences about the game known to be true
        self.knowledge = []

    def mark_mine(self, cell):
        """
        Marks a cell as a mine, and updates all knowledge
        to mark that cell as a mine as well.
        """
        self.mines.add(cell)
        for sentence in self.knowledge:
            sentence.mark_mine(cell)

    def mark_safe(self, cell):
        """
        Marks a cell as safe, and updates all knowledge
        to mark that cell as safe as well.
        """
        self.safes.add(cell)
        for sentence in self.knowledge:
            sentence.mark_safe(cell)
    
    def neighborhood(self, cell,count):
        """
        Added method: This returns a new Sentece  that has  the set of the neighbors
         of a given cell and the count of mine around that cell based on the knowledge base  
        """
        new_set=set()
        new_count=count

        #this loop creates a set of the naighbors of cell (PUEDO PONER ESTO EN UN METODO)
        #the set of naighborns is made of cells cells whose state is still undetermined
        for i in range(cell[0]-1,cell[0]+2):
            for j in range(cell[1]-1,cell[1]+2):
                
                if  (i,j) in self.safes:
                    continue #if the cell is  known to be safe we don't include it in the set
                elif (i,j) in self.mines:
                    new_count-=1 #if the cell is a known mine we don't include it and is discounted un the count
                elif 0<= i < self.height and 0<=  j < self.width:
                    new_set.add((i,j)) #if we don know anything about the cell yet and is tn the board we added it
        
        return Sentence(new_set,new_count)
      
    def detect(self):

        """
        Added method: this method  detects mines o safes in the self.knowledge.
        If it finds a mine or a safe cell, it marks it and also cleans  the trash senteces of
        the list self.knowledge (empty seneteces and repeated senteces )
        """

        len_known=len(self.knowledge) 
        changes=False #this variable  indicates if there is changes (i.e found a mine or safe)
      
        #this for go through all the senteces of self,knowledge base
        for p in range(len_known):
            prop=self.knowledge[p]
            
            #if the sentences has mines or safes it will mark them in the knowledge
            if prop.all_mines():

                changes=True
                other_mines=frozenset(prop.known_mines())
                for new_m in other_mines:
                    self.mark_mine(new_m)
            elif prop.all_safes():
               
                changes=True
                other_safes=frozenset(prop.known_safes())
                for new_s in other_safes:
                    self.mark_safe(new_s)
    
        #if there were any changes in the knowledge, 
        #then it will look for empty sentences and repeted senteces and clean de self.knowlede
        if changes:
            
            clean_kb=[]
            for k1 in range(len_known):
                pp1=self.knowledge[k1]
                
                if len(pp1.cells)!=0:
                    
                    isnotrepeted=True #this variable indicate if the sentece k1 is repeted in the knowledge
                    k2=k1+1
                    while isnotrepeted and k2<len_known:
                        pp2=self.knowledge[k2]
                        if pp1==pp2:
                            isnotrepeted=False     
                        
                        k2+=1

                    if isnotrepeted:
                        clean_kb.append(pp1)

            self.knowledge=clean_kb
            #if there were changes on then it will look for safes and mines again. 
            # This way, it will make sure if the changes  reveled new mines or  safe
            self.detect()

    def do_inference(self):
        
        newsentences=list() #this list will save the new sentences infered of self.knowledge
        len_known=len(self.knowledge)

        #this cicle will go through all the sentences in self.knowlede and check if there is
        #other sentences that can be infered and add them to the list 'newsentesnces'
        for p1 in range(len_known):
            s1=self.knowledge[p1]

            for p2 in range(p1+1,len_known):
                s2=self.knowledge[p2]
                
                if s1.cells<s2.cells:

                    diff_cell=s2.cells-s1.cells
                    diff_count=s2.count-s1.count
                    infered=Sentence(diff_cell,diff_count)

                    if not infered in self.knowledge: 
                        newsentences=[infered]+newsentences
                elif s2.cells<s1.cells:
                    
                    diff_cell=s1.cells-s2.cells
                    diff_count=s1.count-s2.count
                    infered=Sentence(diff_cell,diff_count)

                    if not infered in self.knowledge:
                        newsentences=[infered]+newsentences
                        
        len_ns= len(newsentences)
        #if there are newsenteces, it adds them to the self. knowled, check if there
        #are mines or safe and tries to check if new senteces can be infered
        if len_ns!=0: 

            self.knowledge=newsentences+self.knowledge

            self.detect()
            self.do_inference()
      
    def add_knowledge(self, cell, count):
        """
        Called when the Minesweeper board tells us, for a given
        safe cell, how many neighboring cells have mines in them.

        This function should:
            1) mark the cell as a move that has been made
            2) mark the cell as safe
            3) add a new sentence to the AI's knowledge base
               based on the value of `cell` and `count`
            4) mark any additional cells as safe or as mines
               if it can be concluded based on the AI's knowledge base
            5) add any new sentences to the AI's knowledge base
               if they can be inferred from existing knowledge
        """

        # STEP 1: mark the cell as a move that has been made
        self.moves_made.add(cell)

        # STEP 2: mark the cell as safe.
        #This condition take action depending on if the cell was already marked as safe or not
        if not cell in self.safes:

            #if the cell wasn't safe it marks it  and creat a new sentece
            self.mark_safe(cell)
            new_sentece= self.neighborhood(cell,count)  # Use addded method to creat a new sentece with the neighbors of the given cell 
            
            #STEP 3
            #If the sentece is not empty and has knew info, it add it to the self.knowledge
            if len(new_sentece.cells)!=0 and not new_sentece in self.knowledge:
                self.knowledge=[new_sentece]+self.knowledge
         
            #STEP 4 AND 5
            #it proceeds to do analysis of the self.knowledge
            self.detect() #looks for sentece that have mines of safes
            self.do_inference() #do inference about the sentences
        else:

            #if the cells was already marked as safe we just creat a new sentece
            new_sentece= self.neighborhood(cell,count)  # Use addded method to creat a new sentece with the neighbors of the given cell 

            # STEP 3
            #if the sentence is not empy and has new info we add it to the knowledge
            if len(new_sentece.cells)!=0 and not new_sentece in self.knowledge:
                
                self.knowledge=[new_sentece]+self.knowledge

                #STEP 4
                #if the new senteces added has mines or safe  it will start the 
                #analysis of the list self.knowledge using self.detect()
                if new_sentece.all_mines() or new_sentece.all_safes():
                    self.detect() 
                
                #STEP 5
                # Then it would try to make inferences of the sentence in self.knowledge
                self.do_inference()

    def make_safe_move(self):
        """
        Returns a safe cell to choose on the Minesweeper board.
        The move must be known to be safe, and not already a move
        that has been made.

        This function may use the knowledge in self.mines, self.safes
        and self.moves_made, but should not modify any of those values.
        """
        len_safe=len(self.safes) #size of the safes set
        len_moved=len(self.moves_made) #size of the set moves_made 

        # if the size of safes and moves_mades are equal then there is no safe moves to be made
        if len_safe==len_moved:
            return None
        else:
            #if there are safe moves to be made we chose one randomly
            safesafe=self.safes-self.moves_made
            safe_chosen=safesafe.pop()
            
            return safe_chosen
        
    def make_random_move(self):
        """
        Returns a move to make on the Minesweeper board.
        Should choose randomly among cells that:
            1) have not already been chosen, and
            2) are not known to be mines
        """
        len_moved=len(self.moves_made)
        len_mines=len(self.mines)
        
        #if the numeber of mines detected + number of moves made = size of the board then
        # there are not moves left to make.
        if len_moved+len_mines==self.height*self.height:
            return None
        else:
            not_made=set()
            for i in range(self.height):
                for j in range(self.width):
                    if not (i,j) in self.moves_made and not (i,j) in self.mines:
                        not_made.add((i,j))

            random_chosen=not_made.pop()
            
            return random_chosen
            