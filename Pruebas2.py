#%%
import random
from minesweeper import Minesweeper,Sentence,MinesweeperAI


#%%
filas=4
colum=4


def detect(conocimientos):

    len_known=len(mai.knowledge)
    print("tama de KB:",len_known)
    #ciclo que recorre la lista knowledge y busca posbiles minas 
    #removel=[]
    changes=False
    for p in range(len_known):
        prop=mai.knowledge[p]
        print("analizo la  prop", prop)
        if prop.all_mines():
            print("todaas son minas")
            #removel.append(p) #indice del elemento a remover
            changes=True
            other_mines=frozenset(prop.known_mines())
            for new_m in other_mines:
                print("la  marco como mina en la original")
                mai.mark_mine(new_m)
                print("mi contador:",other_mines)
                print('lo que se')
                for kn in mai.knowledge:
                    print(kn)
        elif prop.all_safes():
            print("todas son sguras")
            #removel.append(p)
            changes=True
            other_safes=frozenset(prop.known_safes())
            for new_s in other_safes:
                print("la  marco como safe en la original")
                mai.mark_safe(new_s)
                print("mi contador:",other_safes)
                print('lo que se')
                for kn in mai.knowledge:
                    print(kn)
   
    print("Minas",mai.mines)
    print("Seguros",mai.safes)

    #ve si se encontro alguna mina o safe (hubo cambio kn)
    #si hubo cambios en el knoledge base entonces limpiara 
    #la basura que podria surgir de encontrar una mina o un safe
    #es decir, quitara lo conjuntos repetidos u los vacios.
    #Ademas  de haberse encontrado cambio finalmente volvera a buscar
    #Nuevas minas y safes que podieron haber aparecido a raiz de los
    #cambios
    if changes:
        print("hubo cambios...vere si hay clones o vacios")
        clean_kb=[]
        for k1 in range(len_known):
            pp1=mai.knowledge[k1]
            print("reviso", pp1)
            if len(pp1.cells)!=0:
                print("no es vacio")
                isnotrepeted=True
                k2=k1+1
                while isnotrepeted and k2<len_known:
                    pp2=mai.knowledge[k2]
                    if pp1==pp2:
                        isnotrepeted=False
                        print("tiene un clon")
                    k2+=1

                if isnotrepeted:
                    clean_kb.append(pp1)

        mai.knowledge=clean_kb
        print('lo que se')
        for kn in mai.knowledge:
            print(kn)
        
        print("Minas",mai.mines)
        print("Seguros",mai.safes)
        detect(mai)

#%%
def do_inference(conocimientos):
    newsentences=list()
    len_known=len(mai.knowledge)
    for p1 in range(len_known):
        s1=mai.knowledge[p1]
        print("S1: ",s1)
        for p2 in range(p1+1,len_known):
            s2=mai.knowledge[p2]
            print("S2:",s2 )
            
            if s1.cells<s2.cells:
                print("encontro un subconjuto 1")
                diff_cell=s2.cells-s1.cells
                diff_count=s2.count-s1.count
                infered=Sentence(diff_cell,diff_count)

                if not infered in mai.knowledge: #OJO REVISAR
                    print("Afiarmcion inferida",infered)
                    newsentences.append(infered) 

            elif s2.cells<s1.cells:
                print("encontro un subconjuto 2")
                diff_cell=s1.cells-s2.cells
                diff_count=s1.count-s2.count
                infered=Sentence(diff_cell,diff_count)
                if not infered in mai.knowledge:
                    print("Afiarmcion inferida",infered)
                    newsentences.append(infered) 

    #podro revisar la lista de new senetences  para ver si se inferio alguna celda segura o mina
    #o desejarlo como esta

    len_ns= len(newsentences)
    if len_ns!=0:
        print("hubo inferencias nuevas y las agregare")
        mai.knowledge.extend(newsentences)
        print("lo que se")
        for kn in mai.knowledge:
            print(kn)
            
        print("Minas",mai.mines)
        print("Seguros",mai.safes)

        detect(mai)
        #OJO PRODIA INVOCAR AQUI LA INFERCIA DE NUEVO DE FORMA RECURSIVA
        #HACER MAS PRUEBAS (IDEAR CIRUNTACIA)
        #doinference(mai)

#%%
mai=MinesweeperAI(filas,colum)

print("---------------Primer movimiento---------")
mai.add_knowledge((1,2),2)

print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)

detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 1 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

make_safe=mai.make_safe_move()
if make_safe is None:
    make_random=mai.make_random_move()
    if make_random is None:
        print("no se puede hacer ningun movimiento")
    else:
        print("moviiento random propuesto", make_random)
else:
    print("movimiento seguro propuesto", make_safe)


print("---------segundo Movimiento---------------------")
mai.add_knowledge((3,1),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)  
print("Minas",mai.mines)
print("Seguros",mai.safes)

detect(mai)
do_inference(mai)
print("AL TERMINAS DE ANILIZAR MOV. 2 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)
 
print("---------Teercer Movimiento---------------------")
mai.add_knowledge((3,3),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn) 
print("Minas",mai.mines)
print("Seguros",mai.safes)

detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 3 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)



print("---------CUARTO Movimiento---------------------")

mai.add_knowledge((0,3),0)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)

detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 4 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

print("---------QUINTO Movimiento---------------------")

mai.add_knowledge((0,2),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)


detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 5 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)


print("---------SEXTO Movimiento---------------------")

mai.add_knowledge((1,3),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 6 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

print("---------SEPTIMO Movimiento---------------------")

mai.add_knowledge((2,1),2)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 7 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

print("---------OCTAVO Movimiento---------------------")

mai.add_knowledge((3,2),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 8 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

print("---------NOVENO Movimiento---------------------")

mai.add_knowledge((1,0),1)
print("Movimientos realizados", mai.moves_made)
print("lo que se")
for kn in mai.knowledge:
     print(kn)
    
print("Minas",mai.mines)
print("Seguros",mai.safes)


detect(mai)
do_inference(mai)

print("AL TERMINAS DE ANILIZAR MOV. 9 SE TIENE:")
for kn in mai.knowledge:
    print(kn)   
print("Minas",mai.mines)
print("Seguros",mai.safes)
print("Movimientos hechos:", mai.moves_made)

